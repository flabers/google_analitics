<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('google/', 'GoogleController@index');
Route::get('google/total/', 'GoogleController@formTotal');
Route::post('google/total/', 'GoogleController@getReportAllProjects');
Route::post('google/', 'GoogleController@getAnalytics');

Auth::routes();
Route::get('redis1', function (){
    Redis::connect();
    Redis::set("key", "value");
    return Redis::get("key");

});
Route::get('redis', 'redisController@showProfile');
Route::get('payPal', 'payPalController@');

Route::get('/home', 'HomeController@index')->name('home');
Route::auth();

Route::group(['prefix'=>'admin', 'middleware'=>['web', 'auth']], function(){
    Route::get('/', ['uses'=>'AdminController@show', 'as'=>'admin_index']);
    Route::get('/sites', ['uses'=>'AdminController@sites', 'as'=>'admin_sites']);
    //sites goals
    Route::post('/admin/add_site', 'AdminController@site_add');
    Route::get('/sites/delete/{id}', 'AdminController@sites_delete');
    Route::get('/sites/edit/{id}', 'AdminController@show_sites_edit');
    Route::post('/sites/edit_site/{id}', 'AdminController@sites_edit');
    Route::get('/goals', ['uses'=>'AdminController@goals', 'as'=>'admin_goals']);

    //goals goals
    Route::post('/admin/add_goal', 'AdminController@goal_add');
    Route::get('/goals/delete/{id}', 'AdminController@goal_delete');
    Route::get('/goals/edit/{id}', 'AdminController@show_goals_edit');
    Route::post('/goals/edit_goal/{id}', 'AdminController@goal_edit');
    Route::get('/goals', ['uses'=>'AdminController@goals', 'as'=>'admin_goals']);
});
