@include('form')

<table class="table">
    <tr style=''>
        <td>Google Adwords</td>
        <td>Затраты</td>
        <td>Клики</td>
        <td>Стоимость клика, грн</td>
        <td>Показы</td>
        <td>CTR, %</td>
        <td>Транзакции</td>
        <td>Конверсия, %</td>
        <td>Стоимость привлечения, грн</td>
    </tr>

        @if(count($results["google_search"]) > 0)
            <tr>
                <td>Google Search</td>
            @foreach($results["google_search"] as $result)
                <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
            @endforeach
            </tr>
            @if(count($results["compagines_search"]) > 0)
            <div class="compagens">
                @foreach($results["compagines_search"] as $name_compagen => $result)
                <tr>
                    <td>{{$name_compagen}}</td>
                    @foreach($result as $metrik => $value)
                        <td>{{$value["current"]}} {!!$value["past"]!!}</td>
                    @endforeach
                </tr>
                @endforeach
            </div>
            @endif
        @endif

    @if(count($results["media_google"]) > 0)
        <tr>
            <td>Google media</td>
            @foreach($results["media_google"] as $result)
                <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
            @endforeach
        </tr>
        @if(count($results["compagines_media"]) > 0)
            <div class="compagens">
            @foreach($results["compagines_media"] as $name_compagen => $result)
                <tr>
                    <td>{{$name_compagen}}</td>
                    @foreach($result as $metrik => $value)
                        <td>{{$value["current"]}} {!!$value["past"]!!}</td>
                    @endforeach
                </tr>
            @endforeach
            </div>
        @endif
    @endif

    @if(count($results["total_result"]) > 0)
        <tr>
            <td>Total</td>
            @foreach($results["total_result"] as $result)
                <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
            @endforeach
        </tr>
    @endif

    @if(count($results["organic_search"]) > 0)
        <tr>
            <td>Google Organic</td>
            @foreach($results["organic_search"] as $result)
                <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
            @endforeach
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        @if(count($results["organic_compagens"]) > 0)
            <div class="compagens">
            @foreach($results["organic_compagens"] as $compagen_name => $result)
                @if($result["current"] != 0)
                <tr>
                    <td>{{$compagen_name}}</td>
                    <td>{{$result["current"]}} {!! $result["past"] !!}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </div>
                @endif
            @endforeach
        @endif


    @endif

</table>
@if(count($goals) > 0)

<h2>Цели:</h2>
<p>
    <ul>
        @foreach($goals as $goal)
            <li>{{$goal}}</li>
        @endforeach
    </ul>
</p>
@endif

@include('footer')
