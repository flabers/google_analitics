@include('header')
@if(count($projects) > 0)
<form class="form" action="" method="POST">
    <div class="form-group">

        <label for="site"></label>

        <select name="site" id="site" class="form-control">
            @foreach($projects as $project)
            <option value="{{$project->name}}"
                    @isset($site_name)
                        @if($site_name == $project->name)
                            selected
                        @endif
                    @endisset >
                {{$project->name}}
            </option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="">Выберите начальную дату
            <input class="form-control" type="date" name="data_start" placeholder="2017-05-24" value="@isset($period){{$period["start"]}}@endisset"> </label>
        <label for="">Выберите конечную дату
            <input class="form-control" type="date" name="data_end" placeholder="2017-06-23" value="@isset($period){{$period["end"]}}@endisset"> </label>
        <label class="checkbox" for="one_month">
            <input type="checkbox" name="one_month" id="one_month"/> Один месяц </label>
        <label class="checkbox" for="one_week">
            <input type="checkbox" name="one_week" id="one_week"/> Одна неделя </label>
        <label class="checkbox" for="today">
            <input type="checkbox" name="today" id="today"/> Сегодня </label>
        <label class="checkbox" for="yesterday">
            <input type="checkbox" name="yesterday" id="yesterday"/> Вчера </label>
{{--        <label class="checkbox" for="save">
            <input type="checkbox" name="save" id="save"> Сохранить отчет в exel формате</label>--}}
        <label class="checkbox" for="mail">
            <input type="checkbox" name="mail" id="mail"> Отослать на почту</label>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input class="submit btn btn-default" type="submit" value="Получить отчет" name="submit"> </div>
</form>
    @else
     <p>На даный момент нет активных проектов</p>
        @endif