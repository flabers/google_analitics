@include('admin.head')
<div class="container">
    @include('admin.menu')
<h2>Edit site</h2>
<p><a href="{{route('admin_sites')}}">Назад</a></p>
<div class="edit_site">
<form action="{{route('admin_sites')}}/edit_site/{{$site[0]->id}}" method="POST">
    Site name:<br>
    <input class="form-control" type="text" name="site_name" value="{{$site[0]->name}}">
    <br>
    Site id:<br>
    <input class="form-control" type="text" name="site_id" value="{{$site[0]->account_id}}">
    <br>
    Cost organic
    <input class="form-control" type="text" name="cost_organic" value="{{$site[0]->cost_organic}}">
    <br>
    <br>Email<br>
    <input class="form-control" type="text" name="email" value="{{$site[0]->email}}">
   <br> Active<br>
    <input class="form-control" type="checkbox" name="active" @if($site[0]->active == 1)
            checked
            @endif
    >
    <input type="hidden" name="_token" value="{{ csrf_token() }}"><br>
    <input class="form-control" type="submit" value="Submit">
</form>
</div>
</div>
@include('admin.footer')
