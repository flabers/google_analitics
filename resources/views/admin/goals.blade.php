@include('admin.head')
		
					<h1 id="head">Steal My Admin Template</h1>

@include('admin.menu')
		
			<div id="content" class="container_16 clearfix">
				<div class="grid_16">
					<p>
						<h1>Add new goal or edit existing</h1>
					</p>
				</div>
				<h2>Add new goals</h2>
				@isset($msg)
					{{$msg}}
				@endisset
				<form action="admin/add_goal" method="POST">
					Goal name:<br>
					<input class="form-control" type="text" name="goal_name" value="{{old('goal_name')}}">
					<br>
					Goal id:<br>
					<input class="form-control"  type="text" name="goal_id" value="{{old('goal_goal')}}">
					<br>
					<input class="form-control" type="text" name="account_id" value="{{old('account_id')}}">
					<br>
					<textarea class="form-control" name="goal_desc" id="" cols="30" rows="10"></textarea><br>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input class="form-control" type="submit" value="Submit">
				</form>
				<br>

				<div class="grid_16">
					<table>
						<thead>
							<tr>
								<th>Account id</th>
								<th>Goal name</th>
								<th>Goal id</th>
								<th>Goal description</th>
								<th>Goal edit</th>
								<th>Goal delete</th>

							</tr>
						</thead>
{{--						<tfoot>
							<tr>
								@if($goals)
								<td colspan="5" class="pagination">
									<span class="active curved">1</span><a href="#" class="curved">2</a><a href="#" class="curved">3</a><a href="#" class="curved">4</a> ... <a href="#" class="curved">10 million</a>
								</td>
									@endif
							</tr>
						</tfoot>--}}
						<tbody>
						@if($goals)
							@foreach($goals as $goal)
							<tr>
								<td>{{$goal->account_id}}</td>
								<td>{{$goal->name_goal}}</td>
								<td>{{$goal->goal_id}}</td>
								<td>{{$goal->goal_description}}</td>
								<td><a href="{{route('admin_goals')}}/edit/{{$goal->id}}" class="edit">Edit</a></td>
								<td><a href="{{route('admin_goals')}}/delete/{{$goal->id}}" class="delete">Delete</a></td>

							</tr>
							@endforeach
						@endif
						</tbody>
					</table>
				</div>
			</div>

@include('admin.footer')