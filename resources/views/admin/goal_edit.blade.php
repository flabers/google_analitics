@include('admin.head')
@include('admin.menu')
<div class="container">
<h2>Edit goal</h2>
    <p><a href="{{route('admin_goals')}}">Назад</a></p>
@if($goal)
<form action="{{route('admin_goals')}}/edit_goal/{{$goal[0]->id}}" method="POST">
    Goal name:<br>
    <input class="form-control" type="text" name="goal_name" value="{{$goal[0]->name_goal}}">
    <br>
    Goal id:<br>
    <input class="form-control" type="text" name="goal_id" value="{{$goal[0]->goal_id}}">
    <br>
    <input class="form-control" type="text" name="account_id" value="{{$goal[0]->account_id}}">
    <br>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input class="form-control" type="submit" value="Submit">

</form>
    @else
    <p>Here are not goal yet</p>
</div>
@endif
@include('admin.footer')
