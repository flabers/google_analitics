@include('admin.head')
	<h1 id="head">Здесь отображаются все активные цели и форма для добалвения новых</h1>
@include('admin.menu')

			<div id="content" class="container_16 clearfix">
				<div class="grid_16">
					<h2>Add new site</h2>
					<form action="admin/add_site" method="POST">
						Site name:<br>
						<input type="text" name="site_name" value="{{old('site_name')}}">
						<br>
						Site id:<br>
						<input type="text" name="site_id" value="{{old('site_id')}}">
						<br>
						<br>Cost organic<br>
						<input type="text" name="cost_organic" value="{{old('cost_organic')}}">
						<br>Email<br>
						<input type="text" name="email" value="{{old('cost_organic')}}">
						<br>Active<br>
						<input type="checkbox" name="active" >
						<br>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="submit" value="Submit">
					</form>
				</div>
			</div>
@if($sites)
	<div class="container container_16 clearfix">
		<h2>Текушие проекты</h2>
	<table class="table">
		<tr>
			<td>Name</td>
			<td>Account id (from analitics)</td>
			<td>time created</td>
			<td>Cost organic</td>
			<td>Email</td>
			<td>Active</td>
			<td>Edit</td>
			<td>Удалить</td>
		</tr>
	@foreach($sites as $site)
		<tr>
			<td>{{$site->name}}</td>
			<td>{{$site->account_id}}</td>
			<td>{{$site->created_at}}</td>
			<td>{{$site->cost_organic}}</td>
			<td>{{$site->email}}</td>
			<td>
				@if($site->active == 1)
					On
				@else
					off
				@endif
			</td>
			<td><a href="{{route('admin_sites')}}/edit/{{$site->id}}">Edit</a></td>
			<td><a href="{{route('admin_sites')}}/delete/{{$site->id}}">Удалить</a></td>
		</tr>
	@endforeach
	</table>
	</div>
@endif
@include('admin.footer')