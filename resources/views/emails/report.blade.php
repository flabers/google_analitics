<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>


<table class="table">
    <tr style=''>
        <td>Google Adwords</td>
        <td>Затраты</td>
        <td>Клики</td>
        <td>Стоимость клика, грн</td>
        <td>Показы</td>
        <td>CTR, %</td>
        <td>Транзакции</td>
        <td>Конверсия, %</td>
        <td>Стоимость привлечения, грн</td>
    </tr>


    @if(count($results["google_search"]) > 0)
        <tr>
            <td>Google Search</td>
            @foreach($results["google_search"] as $result)
                <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
            @endforeach
            {{--                @foreach($results["compagines_search"] as $result)
                                <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
                            @endforeach--}}
        </tr>
    @endif

    @if(count($results["organic_search"]) > 0)
        <tr>
            <td>Google Organic</td>
            @foreach($results["organic_search"] as $result)
                <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
            @endforeach
            {{--            @foreach($results["organic_compagens"] as $result)
                            <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
                        @endforeach--}}
        </tr>
    @endif
</table>
{{--@if(count($goals) > 0)
    <h2>Цели:</h2>
    <p>
    <ul>
        @foreach($goals as $goal)
            <li>{{$goal}}</li>
        @endforeach
    </ul>
    </p>
@endif--}}
@include('footer')
</body>
</html>