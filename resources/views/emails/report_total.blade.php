@include('form_total')

<table class="table">
    <tr style=''>
        <td>Google Adwords</td>
        <td>Затраты</td>
        <td>Клики</td>
        <td>Стоимость клика, грн</td>
        <td>Показы</td>
        <td>CTR, %</td>
        <td>Транзакции</td>
        <td>Конверсия, %</td>
        <td>Стоимость привлечения, грн</td>
    </tr>

    @if(count($results) > 0)
        @foreach($results as $project => $one_project)
            <tr>
                <td>{{$project}}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @if(count($one_project["google_search"]) > 0)
                <tr>
                    <td>Google Search</td>
                    @foreach($one_project["google_search"] as $result)
                        <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
                    @endforeach
                </tr>
                @if(count($one_project["compagines_search"]) > 0)
                    @foreach($one_project["compagines_search"] as $name_compagen => $result)
                        <tr>
                            <td>{{$name_compagen}}</td>
                            @foreach($result as $metrik => $value)
                                <td>{{$value["current"]}} {!!$value["past"]!!}</td>
                            @endforeach
                        </tr>
                    @endforeach
                @endif
            @endif

            @if(count($one_project["media_google"]) > 0)
                <tr>
                    <td>Google media</td>
                    @foreach($one_project["media_google"] as $result)
                        <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
                    @endforeach
                </tr>
                @if(count($one_project["compagines_media"]) > 0)
                    @foreach($one_project["compagines_media"] as $name_compagen => $result)
                        <tr>
                            <td>{{$name_compagen}}</td>
                            @foreach($result as $metrik => $value)
                                <td>{{$value["current"]}} {!!$value["past"]!!}</td>
                            @endforeach
                        </tr>
                    @endforeach
                @endif
            @endif

            @if(count($one_project["total_result"]) > 0)
                <tr>
                    <td>Total</td>
                    @foreach($one_project["total_result"] as $result)
                        <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
                    @endforeach
                </tr>
            @endif

            @if(count($one_project["organic_search"]) > 0)
                <tr>
                    <td>Google Organic</td>
                    @foreach($one_project["organic_search"] as $result)
                        <td class="sheet"><span class="current">{{$result["current"]}}</span> {!! $result["past"] !!}</td>
                    @endforeach
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @if(count($one_project["organic_compagens"]) > 0)
                    @foreach($one_project["organic_compagens"] as $compagen_name => $result)
                        @if($result["current"] != 0)
                            <tr>
                                <td>{{$compagen_name}}</td>
                                <td>{{$result["current"]}} {!! $result["past"] !!}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                    @endforeach
                @endif


            @endif
        @endforeach
</table>
@if(count($goals) > 0)

    <h2>Цели:</h2>
    <p>
    <ul>
        @foreach($goals as $name_goal => $goals_one)
            <h3>{{$name_goal}}</h3>
            <ul>
                @foreach($goals_one as $goal)
                    @if(strlen($goal) > 0)
                        <li>{{$goal}}</li>
                    @else
                        <li>Goals did`t set</li>
                    @endif
                @endforeach
            </ul>
        @endforeach
    </ul>
    </p>
@endif

@endif
@include('footer')
