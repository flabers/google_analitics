<?php
namespace App\Helpers\Contracts;
use Illuminate\Http\Request;
Interface GoogleApiMethod{

    public static function getOrganicSearch(Request $request);
    public static function getGoogleAdwords(Request $request);
}