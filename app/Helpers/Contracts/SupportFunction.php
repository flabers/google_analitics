<?php
namespace App\Helpers\Contracts;
use Illuminate\Http\Request;
Interface SupportFunction{
    public function __construct();
    public static function getDataPeriod($request);
    public static function fromQueryToTableGoogleAnalytics($array);
    public static function getGoalsFromDb($goals);
    public static function getCTR($click, $impress);
    public static function getCPC($cost, $click);
    public static function getConversion($trans, $click);
    public static function getCostAttraction($cost, $trans);
    public static function summ($var1, $var2);
    public static function sumDivide($var1, $var2);
    public static function getPeriodForComparesin($period, $query);
    public static function getUnitedResult($array1, $array2);
    public static function giveColorClassToPastResultsPositive($val1, $val2, $obj);
    public static function giveColorClassToPastResultsNegative($val1, $val2, $obj);
}