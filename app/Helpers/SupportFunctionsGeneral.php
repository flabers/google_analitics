<?php
namespace App\Helpers;
use App\Helpers\Contracts\SupportFunction;
use Illuminate\Http\Request;
use Spatie\Analytics\Period;
use Illuminate\Support\Facades\DB;
use DateTime;
//use Session;
class SupportFunctionsGeneral implements SupportFunction{

    protected  $allResults;

    function __construct(){
        $this->allResults = [];
    }
 /*   take results from google api query and make array of results*/

    public static  function fromQueryToTableGoogleAnalytics($array){
        $obj = new self();
        $compagens = [];
    $result = [
        'cost_search_ad'=>0,
        'impressions_search_ad'=>0,
        'click_search_ad'=>0,
        'oneclick'=>0,
        'full_order'=>0,
        'transaction_search'=>0
    ];

        $result_media = [
            'cost_search_ad'=>0,
            'impressions_search_ad'=>0,
            'click_search_ad'=>0,
            'oneclick'=>0,
            'full_order'=>0,
            'transaction_search'=>0
        ];
        $total_result = [
            'cost_search_ad'=>0,
            'impressions_search_ad'=>0,
            'click_search_ad'=>0,
            'oneclick'=>0,
            'full_order'=>0,
            'transaction_search'=>0
        ];
        $result_compagens = [];

        $result_compagens_media = [];

    $result_organic = [
        'google_organic' => 0
    ];

    for($i=0; $i<count($array); $i++){
        if( preg_match("~Google Search~i", $array[$i][0]) || preg_match("~Search partners~i", $array[$i][0])){

            //informations for table
            $result['cost_search_ad'] +=  $array[$i][2];
            $result['click_search_ad'] += $array[$i][4];
            $result['impressions_search_ad'] += $array[$i][3];
            $result['oneclick'] +=   $array[$i][6];
            if(isset($array[$i][7])) {
                $result['full_order'] += $array[$i][7];
                $result['transaction_search'] += $array[$i][7] + $array[$i][6];
            }else{
                $result['full_order'] += 0;
                $result['transaction_search'] += 0;
            }
            //$result['compagen_name'] = $array[$i][1];
            //informations for compagens table

            $result_compagens[$array[$i][1]]['google_search'] = $result;


        }elseif(preg_match("~Content~i", $array[$i][0]) ){
            $result_media['cost_search_ad'] +=  $array[$i][2];
            $result_media['click_search_ad'] += $array[$i][4];
            $result_media['impressions_search_ad'] += $array[$i][3];
            $result_media['oneclick'] +=   $array[$i][6];
            if(isset($array[$i][7])){
                $result_media['full_order'] +=   $array[$i][7];
                $result_media['transaction_search'] +=   $array[$i][7] + $array[$i][6];
            }else{
                $result_media['full_order'] +=   0;
                $result_media['transaction_search'] +=   0;
            }
            //compagens
            $result_compagens_media[$array[$i][1]]['google_media'] = $result;
        }
        if(isset($array[$i][8])){
            $result_organic['google_organic'] += $array[$i][8];
            $result_compagens[$array[$i][1]]['google_organic'] = $array[$i][8];
        }else{
            $result_organic['google_organic'] += 0;
            $result_compagens[$array[$i][1]]['google_organic'] = 0;
        }

    }
        //main table

        $result['ctr'] = $obj->getCTR($result['click_search_ad'], $result['impressions_search_ad']);
        $result['conversion'] = $obj->getConversion($result['transaction_search'], $result['click_search_ad']);
        $result['attraction'] = $obj->getCostAttraction($result['cost_search_ad'], $result['transaction_search']);
        $result['cpc'] = $obj->getCPC( $result['cost_search_ad'], $result['click_search_ad']);
        unset($result['oneclick']);
        unset($result['full_order']);
        //media ad google

        $result_media['ctr'] = $obj->getCTR($result_media['click_search_ad'], $result_media['impressions_search_ad']);
        $result_media['conversion'] = $obj->getConversion($result_media['transaction_search'], $result_media['click_search_ad']);
        $result_media['attraction'] = $obj->getCostAttraction($result_media['cost_search_ad'], $result_media['transaction_search']);
        $result_media['cpc'] = $obj->getCPC( $result_media['cost_search_ad'], $result_media['click_search_ad']);
        unset($result_media['oneclick']);
        unset($result_media['full_order']);

        //comapagens




        // iterates through the array to add additional

        foreach ($result_compagens as $key => $v){
            foreach ($v as $k => $val){
                if($k == 'google_search'){
                    $result_compagens[$key]["google_search"]["ctr"] = $obj->getCTR($val["click_search_ad"], $val["impressions_search_ad"]);
                    $result_compagens[$key]["google_search"]["conversion"] = $obj->getConversion($val["transaction_search"], $val["click_search_ad"]);
                    $result_compagens[$key]["google_search"]["attraction"] = $obj->getCostAttraction($val["cost_search_ad"], $val["transaction_search"]);
                    $result_compagens[$key]["google_search"]["cpc"] = $obj->getCPC( $val["cost_search_ad"], $val["click_search_ad"]);
                    unset($result_compagens[$key]["google_search"]["oneclick"]);
                    unset($result_compagens[$key]["google_search"]["full_order"]);

                }

            }

        }
        foreach ($result_compagens_media as $key => $v){
            foreach ($v as $k => $val){
                if($k == 'google_media'){
                    $result_compagens_media[$key]["google_media"]["ctr"] = $obj->getCTR($val["click_search_ad"], $val["impressions_search_ad"]);
                    $result_compagens_media[$key]["google_media"]["conversion"] = $obj->getConversion($val["transaction_search"], $val["click_search_ad"]);
                    $result_compagens_media[$key]["google_media"]["attraction"] = $obj->getCostAttraction($val["cost_search_ad"], $val["transaction_search"]);
                    $result_compagens_media[$key]["google_media"]["cpc"] = $obj->getCPC( $val["cost_search_ad"], $val["click_search_ad"]);
                    unset($result_compagens_media[$key]["google_media"]["oneclick"]);
                    unset($result_compagens_media[$key]["google_media"]["full_order"]);

                }

            }

        }

        $result = [
            $result["cost_search_ad"],
            $result["click_search_ad"],
            $result["cpc"],
            $result["impressions_search_ad"],
            $result["ctr"],
            $result["transaction_search"],
            $result["conversion"],
            $result["attraction"],
        ];
        //media ad google
        $result_media = [
            $result_media["cost_search_ad"],
            $result_media["click_search_ad"],
            $result_media["cpc"],
            $result_media["impressions_search_ad"],
            $result_media["ctr"],
            $result_media["transaction_search"],
            $result_media["conversion"],
            $result_media["attraction"],
        ];
        $total_result = [
            $total_result["cost_search_ad"] =  $result[0] +  $result_media[0],
            $total_result["click_search_ad"] = $result[1] +  $result_media[1],
            $total_result["cpc"] = ($result[2] +  $result_media[2]) / 2,
            $total_result["impressions_search_ad"] = $result[3] +  $result_media[3],
            $total_result["ctr"] = ($result[4] +  $result_media[4]) / 2,
            $total_result["transaction_search"] = $result[5] +  $result_media[5],
            $total_result["conversion"] = $result[6] +  $result_media[6],
            $total_result["attraction"] = ($result[7] +  $result_media[7]) / 2,
        ];
        // resort comagen`s data
            foreach ($result_compagens as $name => $compagen){
                if(isset($compagen["google_search"])){
                    $result_compagens[$name]["google_search"] = [
                        $result_compagens[$name]["google_search"]["cost_search_ad"],
                        $result_compagens[$name]["google_search"]["click_search_ad"],
                        $result_compagens[$name]["google_search"]["cpc"],
                        $result_compagens[$name]["google_search"]["impressions_search_ad"],
                        $result_compagens[$name]["google_search"]["ctr"],
                        $result_compagens[$name]["google_search"]["transaction_search"],
                        $result_compagens[$name]["google_search"]["conversion"],
                        $result_compagens[$name]["google_search"]["attraction"],
                    ];
                }
            }

        foreach ($result_compagens_media as $name => $compagen){
            if(isset($compagen["google_media"])){
                $result_compagens_media[$name]["google_media"] = [
                    $result_compagens_media[$name]["google_media"]["cost_search_ad"],
                    $result_compagens_media[$name]["google_media"]["click_search_ad"],
                    $result_compagens_media[$name]["google_media"]["cpc"],
                    $result_compagens_media[$name]["google_media"]["impressions_search_ad"],
                    $result_compagens_media[$name]["google_media"]["ctr"],
                    $result_compagens_media[$name]["google_media"]["transaction_search"],
                    $result_compagens_media[$name]["google_media"]["conversion"],
                    $result_compagens_media[$name]["google_media"]["attraction"],
                ];
            }
        }
        // end resort
        $obj->allResults['google_search'] = $result;
        $obj->allResults['organic_search'] = $result_organic;
        $obj->allResults['compagines'] = $result_compagens;
        $obj->allResults['compagines_media'] = $result_compagens_media;
        $obj->allResults['media_google'] = $result_media;
        $obj->allResults['total_result'] = $total_result;

    return $obj->allResults;
}
/*take datas from form and make DataTime insctance for google api query*/
   public static function getDataPeriod($request){
       if((isset($request["data_start"]) AND isset($request["data_end"])) AND
           (!isset($request["one_week"]) AND !isset($request["one_week"]) AND !isset($request["one_month"]) AND !isset($request["yesterday"])
           AND !isset($request["today"])
           )){
           $startDate = $request["data_start"];
           $endDate = $request["data_end"];
           $period =  Period::create($dataTimeObject = new DateTime($startDate), $dataTimeObject = new DateTime($endDate));
       }elseif(isset($request["one_week"])){
           $period = Period::days(7);
       }elseif(isset($request["one_month"])){
           $startDate = date('Y-m-d', strtotime('-1 month'));
           $endDate = date('Y-m-d');
           $period =  Period::create($dataTimeObject = new DateTime($startDate), $dataTimeObject = new DateTime($endDate));
       }elseif(isset($request["yesterday"])){
           $startDate = date('Y-m-d', strtotime('-1 day'));
           $endDate = date('Y-m-d', strtotime('-1 day'));
           $period =  Period::create($dataTimeObject = new DateTime($startDate), $dataTimeObject = new DateTime($endDate));
       }elseif(isset($request["today"])){
           $startDate = date('Y-m-d');
           $endDate = date('Y-m-d');
           $period =  Period::create($dataTimeObject = new DateTime($startDate), $dataTimeObject = new DateTime($endDate));
       }else{
           $startDate = date('Y-m-d', strtotime('-1 month'));
           $endDate = date('Y-m-d');
           $period =  Period::create($dataTimeObject = new DateTime($startDate), $dataTimeObject = new DateTime($endDate));
           $obg =  new self();

       }
       return $period;
   }
  /* get name of all goals from DB*/
   public static function getGoalsFromDb($goals){
       $goals_str = '';
       if(count($goals) > 0){
            foreach ($goals as $goal){
                $goals_str .= ','.$goal->goal_id;
            }
           return $goals_str;
        }
        return $goals_str;
   }
   public static function getCTR($click, $impress){
       if( $impress != 0){
           $ctr = round((($click / $impress) * 100), 1);
           return $ctr;
       }
       return false;
   }
   public static function getCPC($cost, $click){
       if($click != 0){
           $cpc = round(($cost / $click), 1);
           return $cpc;
       }
    return false;
   }
   public static function getConversion($trans, $click){
       if($click != 0){
           $convert = round((($trans / $click) * 100), 1);
       }else{
           $convert = 0;
       }
       return $convert;
   }
   public static function getCostAttraction($cost, $trans){
       if( $trans != 0){
           $attraction = round(($cost / $trans), 2);
       }else{
           $attraction = 0;
       }

       return $attraction;
   }
   public static function summ($var1, $var2){
       $sum = $var1 + $var2;
       return round($sum,2);
   }
   public static function sumDivide($var1, $var2){
       $sum = (($var1 + $var2) / 2);
       return round($sum,2);
   }
   protected function cleanUpResultFromComa($str){
       if(strlen($str)>0){
           $str = preg_replace("~,$~", '', $str);
       }
       return $str;

   }
   public static function getPeriodForComparesin($period, $query){
       if(isset($query['today'])){
           $startDate = date('Y-m-d', strtotime('-1 day'));
           $endDate = date('Y-m-d', strtotime('-1 day'));
           $period =  Period::create($dataTimeObject = new DateTime($startDate), $dataTimeObject = new DateTime($endDate));
           return $period;
       }elseif(isset($query['yesterday'])){
           $startDate = date('Y-m-d', strtotime('-2 day'));
           $endDate = date('Y-m-d', strtotime('-2 day'));
           $period =  Period::create($dataTimeObject = new DateTime($startDate), $dataTimeObject = new DateTime($endDate));
           return $period;
       }else{
           $newEndData = $period->endDate;
           $newStartData = $period->startDate;
           $diff = $newStartData->diff($newEndData);
           $compare_data =  $newStartData->sub($diff);
           $compare_data =  $newEndData->sub($diff);
           return $period;
       }
   }

   public static function getUnitedResult($array1, $array2){

        $temp = [];
        $obj = new self();
         foreach ($array1 as $key=>$value){
            $temp2 = [];
            if($key == 'google_search'){
                foreach ($value as $k =>$v){
                    $temp3 = [];
                    $temp3['current'] = $v;
                    if(isset($array2['google_search'][$k])) {
                        if ($k == 'cost_search_ad' OR $k == 'cpc') {
                            $temp3['past'] = $obj->giveColorClassToPastResultsNegative($v, $array2['google_search'][$k], $obj);
                        } else {
                            $temp3['past'] = $obj->giveColorClassToPastResultsPositive($v, $array2['google_search'][$k], $obj);
                        }
                    }else{
                        $temp3['past'] = "No data";
                    }
                   $temp2[] = $temp3;
                }
                $temp['google_search'] = $temp2;
            }elseif($key == 'media_google'){
                foreach ($value as $k =>$v){
                    $temp3 = [];
                    $temp3['current'] = $v;
                    if(isset($array2['media_google'][$k])) {
                        if ($k == 'cost_search_ad' OR $k == 'cpc') {
                            $temp3['past'] = $obj->giveColorClassToPastResultsNegative($v, $array2['media_google'][$k], $obj);
                        } else {
                            $temp3['past'] = $obj->giveColorClassToPastResultsPositive($v, $array2['media_google'][$k], $obj);
                        }
                    }else{
                        $temp3['past'] = "No data";
                    }
                    $temp2[] = $temp3;
                }
                $temp['media_google'] = $temp2;
            }elseif($key == 'total_result'){
                foreach ($value as $k =>$v){
                    $temp3 = [];
                    $temp3['current'] = $v;
                    if(isset($array2['total_result'][$k])) {
                        if ($k == 'cost_search_ad' OR $k == 'cpc' OR $k == "attraction") {
                            $temp3['past'] = $obj->giveColorClassToPastResultsNegative($v, $array2['total_result'][$k], $obj);
                        } else {
                            $temp3['past'] = $obj->giveColorClassToPastResultsPositive($v, $array2['total_result'][$k], $obj);
                        }
                    }else{
                        $temp3['past'] = "No data";
                    }
                    $temp2[] = $temp3;
                }
                $temp['total_result'] = $temp2;
            }elseif($key == 'organic_search'){
                foreach ($value as $k =>$v){
                    $temp3 = [];
                    $temp3['current'] = $v;
                    if(isset($array2['organic_search'][$k])) {
                        if ($k == 'cost_search_ad' OR $k == 'cpc' OR $k == "attraction") {
                            $temp3['past'] = $obj->giveColorClassToPastResultsNegative($v, $array2['organic_search'][$k], $obj);
                        } else {
                            $temp3['past'] = $obj->giveColorClassToPastResultsPositive($v, $array2['organic_search'][$k], $obj);
                        }
                    }else{
                        $temp3['past'] = "No data";
                    }
                    $temp2[] = $temp3;
                }
                $temp['organic_search'] = $temp2;
            }elseif($key == 'compagines'){
                foreach ($value as $k =>$v){
                    foreach ($v as $name => $type){
                        if($name == 'google_search'){
                            foreach ($type as $metrik =>$metrik_val){
                                $temp3 = [];
                                $temp3['current'] = $metrik_val;
                                if(isset($array2['compagines'][$k]["google_search"][$metrik])) {
                                    if ($metrik == 'cost_search_ad' OR $metrik == 'cpc') {
                                        $temp3['past'] = $obj->giveColorClassToPastResultsNegative($metrik_val, $array2['compagines'][$k]["google_search"][$metrik], $obj);
                                    } else {
                                        $temp3['past'] = $obj->giveColorClassToPastResultsPositive($metrik_val, $array2['compagines'][$k]["google_search"][$metrik], $obj);
                                    }
                                }else{
                                    $temp3['past'] = "No data";
                                }

                                $array2['compagines'][$k]["google_search"][$metrik] = $temp3;
                                //$temp2[] = $array2['compagines'][$k]["google_search"][$metrik];
                            }
                            $temp['compagines_search'][$k] = $array2['compagines'][$k]["google_search"];
                        }elseif($name == 'google_organic'){
                                $temp3 = [];
                                $temp3['current'] = $type;
                                if(isset($array2['compagines'][$k]["google_organic"])){
                                    //$temp3['name'] = $array2['compagines'][$k]["google_organic"];
                                    $temp3['past'] = $obj->giveColorClassToPastResultsPositive($type, $array2['compagines'][$k]["google_organic"], $obj);
                                }else{
                                    $temp3['past'] = "No data";
                                }
                                $array2['compagines'][$k]["google_organic"] = $temp3;
                                $temp2[] = $temp3;
                            $temp['organic_compagens'][$k] = $array2['compagines'][$k]["google_organic"];
                        }

                        }
                    }
            }elseif($key == 'compagines_media'){
                foreach ($value as $k =>$v){
                    foreach ($v as $name => $type){
                        if($name == 'google_media'){
                            foreach ($type as $metrik =>$metrik_val){
                                $temp3 = [];
                                $temp3['current'] = $metrik_val;
                                if(isset($array2['compagines_media'][$k]["google_media"][$metrik])) {
                                    if ($metrik == 'cost_search_ad' OR $metrik == 'cpc') {
                                        $temp3['past'] = $obj->giveColorClassToPastResultsNegative($metrik_val, $array2['compagines_media'][$k]["google_media"][$metrik], $obj);
                                    } else {
                                        $temp3['past'] = $obj->giveColorClassToPastResultsPositive($metrik_val, $array2['compagines_media'][$k]["google_media"][$metrik], $obj);
                                    }
                                }else{
                                    $temp3['past'] = "No data";
                                }
                                $array2['compagines_media'][$k]["google_media"][$metrik] = $temp3;
                                //$temp2[] = $array2['compagines'][$k]["google_search"][$metrik];
                            }
                            $temp['compagines_media'][$k] = $array2['compagines_media'][$k]["google_media"];
                        }
                    }
                }
            }
        }

       return $temp;

   }
   public static function giveColorClassToPastResultsNegative($val1, $val2, $obj){
       $saldo = ($val1 - $val2);
       if($obj->sign( $saldo ) == '1'){
           if($val1 != 0){
               $saldo = ($saldo / $val1) * 100;
               $saldo = round($saldo,2);
               $saldo = '+'.round($saldo, 2).' %';
               return "<span class='badge pluss'>".$saldo."</span>";
           }
           return '';
       }elseif($obj->sign( $saldo ) == '-1'){
           if($val1 != 0) {
               $saldo = ($saldo / abs($val1)) * 100;
               $saldo = round($saldo, 2);
               $saldo = round($saldo, 2) . ' %';
               return "<span class='badge minus'>" . $saldo . "</span>";
           }
           return '';
       }else{
           if($val1 != 0){
               $saldo = ($saldo / $val1) * 100;
               $saldo = round($saldo,2);
               $saldo = round($saldo, 2).' %';
               return "<span class='badge'>".$saldo."</span>";
           }
            return '';
       }
   }
    public static function giveColorClassToPastResultsPositive($val1, $val2, $obj){
       if($val1 != 0){
           $saldo = ($val1 - $val2);
           if($obj->sign( $saldo ) == '1'){
               $saldo = ($saldo / $val1) * 100;
               $saldo = round($saldo,2);
               $saldo = '+'.round($saldo, 2).' %';
               return " <span class='badge minus'>".$saldo."</span>";
           }elseif($obj->sign( $saldo ) == '-1'){
               $saldo = ($saldo / abs($val1)) * 100;
               $saldo = round($saldo,2);
               $saldo = round($saldo, 2).' %';
               return " <span class='badge pluss'>".$saldo."</span>";
           }else{
               $saldo = ($saldo / $val1) * 100;
               $saldo = round($saldo,2);
               $saldo = round($saldo, 2).' %';
               return " <span class='badge'>".$saldo."</span>";
           }
       }else{
           return '';
       }
    }
    public static function sign( $number ) {
        return ( $number > 0 ) ? 1 : ( ( $number < 0 ) ? -1 : 0 );
    }
    public  function getDateFromDateTimeObject(Period $period, $mail = false){
        $period_array = (array)$period;
        $start_date = (array)$period_array["startDate"];
        $end_date = (array)$period_array["endDate"];
        $start = new DateTime($start_date["date"]);
        $end = new DateTime($end_date["date"]);
        if($mail == true){
            $start = $start->format('d-m-Y');
            $end = $end->format('d-m-Y');
        }else{
            $start = trim($start->format('Y-m-d'));
            $end = trim($end->format('Y-m-d'));
        }
        return ['start' => $start, 'end' =>$end];
    }
 }


?>