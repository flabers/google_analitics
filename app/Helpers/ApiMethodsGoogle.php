<?php

namespace App\Helpers;
use App\Helpers\Contracts\GoogleApiMethod;
use Illuminate\Http\Request;
use Analytics;
use DateTime;
use Spatie\Analytics\Period;
use Illuminate\Support\Facades\DB;
class ApiMethodsGoogle implements GoogleApiMethod
{

    public static function getOrganicSearch(Request $request){
        $obj = new self;
        $query_params =  'ga:adCost, ga:impressions, ga:adClicks, ga:transactions, ga:organicSearches'.','.$obj::goals;
        $dimensions = 'ga:adDistributionNetwork';
        $analyticsData = Analytics::performQuery( $obj::period, $query_params ,             [
            'dimensions' => $dimensions,
        ] );
        return view('table', ['result'=> $analyticsData]);
    }
    public static function getGoogleAdwords(Request $request){
        $obj = new self;

        $query_params =  'ga:organicSearches';
        $dimensions = 'ga:adDistributionNetwork, ga:sourceMedium';
        $analyticsData = Analytics::performQuery($obj::period, $query_params ,             [
            'dimensions' => $dimensions,
        ] );
        return view('table', ['result'=> $analyticsData]);
    }
}
