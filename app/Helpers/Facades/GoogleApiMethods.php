<?php
namespace App\Helpers\Facades;
use Illuminate\Support\Facades\Facade;
class GoogleApiMethods extends Facade{

    protected static function getFacadeAccessor(){
        return 'googleapimethods';
    }
}

?>