<?php 
namespace App\Helpers\Facades;
use Illuminate\Support\Facades\Facade;
class SupportFunctions extends Facade{
    
    protected static function getFacadeAccessor(){
        return 'supportfunction';
    }
}

?>