<?php

namespace App\Providers;

use App\Helpers\SupportFunctionsGeneral;
use Illuminate\Support\ServiceProvider;

class SuportFunctionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('supportfunction', function(){
            return new SupportFunctionsGeneral();
        });
    }
}
