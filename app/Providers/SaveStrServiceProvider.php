<?php

namespace App\Providers;

use App\Helpers\SaveEloquentOrm;
use Illuminate\Support\ServiceProvider;

class SaveStrServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
       // $obj = new SaveStr();
        //$this->app->instance('App\Helpers\Contracts\SaveStr', $obj);
        //если использовать singleton будет использолваться патерт
        $this->app->bind('savestr', function(){
            return new SaveEloquentOrm();
        });
//$this->app->bind('App\Helpers\Contracts\SaveStr', 'App\Helpers\SaveStrEloquentOrm');
    }
}
