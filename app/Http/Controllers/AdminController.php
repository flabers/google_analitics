<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    ///render main admin template  --start
    public function show(){
        return view('admin.template');
    }
    ///render main admin template  --end


    // function for work with site accounts -------start
    public function sites()
    {
        $sites = DB::select('SELECT * FROM  accounts ');
        return view('admin.sites', ['sites'=> $sites]);
    }
    public function site_add(Request $request){
        $site_name = $request->site_name;
        $site_id = $request->site_id;
        $cost_organic = $request->cost_organic;
        if($request->active == "on"){
            $active = 1;
        }else{
            $active = 0;
        }
        $email = $request->email;
        $response = DB::insert('INSERT INTO  accounts SET account_id = ?, name = ?, cost_organic = ?, email = ?, active = ?', [$site_id, $site_name, $cost_organic, $email, $active]);
        return redirect('/admin/sites')->with(['msg'=>'<p class="alert-success">site has been added</p>']);
    }
    public  function  sites_delete($id){
        $deleted = DB::delete('delete from accounts WHERE id = ?', [$id]);
        return redirect('/admin/sites')->with(['msg'=>'<p class="alert-success">site has been deleted</p>']);
    }
    public function show_sites_edit($id){
        $site = DB::select('SELECT * FROM  accounts WHERE id = ?', [$id]);
        return view('admin.site_edit', ['site'=>$site]);
    }
    public function sites_edit($id, Request $request){
        $site_name = $request->site_name;
        $site_id = $request->site_id;
        $cost_organic = $request->cost_organic;
        if($request->active == "on"){
            $active = 1;
        }else{
            $active = 0;
        }
        $email = $request->email;
        $site = DB::update('UPDATE accounts  SET account_id =?, name =?, cost_organic =?, email =?, active =?  WHERE id = ?', [$site_id, $site_name,$cost_organic, $email, $active, $id]);
        return redirect('/admin/sites')->with(['msg'=>'<p class="alert-success">site has been update</p>']);
    }
    // function for work with site accounts -------end


    // function for work with goals  -------start
    public function goals()
    {
        $goals = DB::select('SELECT * FROM  goals ');
        return view('admin.goals', ['goals'=> $goals]);
    }
    public function goal_add(Request $request){
        $goal_name = $request->goal_name;
        $goal_id = $request->goal_id;
        $goal_desc = $request->goal_desc;
        $account_id = $request->account_id;
        $response = DB::insert('INSERT INTO  goals SET goal_id = ?, name_goal = ?, goal_description = ?, account_id = ?', [$goal_id, $goal_name,$goal_desc, $account_id]);
        return redirect('/admin/goals')->with(['msg'=>'goal has been added']);
    }
    public  function  goal_delete($id){
        $deleted = DB::delete('delete from goals WHERE id = ?', [$id]);
        return redirect('/admin/goals')->with(['msg'=>'<p class="alert-success">goal has been deleted</p>']);
    }
    public function show_goals_edit($id){
        $goal = DB::select('SELECT * FROM  goals WHERE id = ?', [$id]);
        return view('admin.goal_edit', ['goal'=>$goal]);
    }
    public function goal_edit($id, Request $request){
        $goal_name = $request->goal_name;
        $goal_id = $request->goal_id;
        $account_id = $request->account_id;
        $goal_description = $request->goal_description;
        $goal = DB::update('UPDATE goals  SET goal_id =?, name_goal =?, account_id = ?, goal_description = ?  WHERE id = ?', [$goal_id, $goal_name,$account_id, $goal_description, $id]);
        return redirect('/admin/goals')->with(['msg'=>'<p class="alert-success">goal has been update</p>']);
    }
    // function for work with goals  -------end



}
