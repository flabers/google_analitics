<?php

namespace App\Http\Controllers;

//use App\Helpers\Contracts\SaveStr;
use Illuminate\Http\Request;
use Analytics;
use DateTime;
use Spatie\Analytics\Period;
use Illuminate\Support\Facades\DB;
use GoogleApiMethods;
use SupportFunctions;
use App;
use SaveStr;
use Mail;

class GoogleController extends Controller
{

    protected $startDate;
    protected $endDate;
    protected $period;
    protected $goals;
    protected $compPeriod;
    protected $projects;

    function __construct()
    {
        $projects = DB::select("SELECT name FROM accounts WHERE active = 1");
        $this->projects = $projects;
    }

    public function getAnalytics(Request $request){
        $name_project = $request->only(['site'])["site"];
        $project_id = DB::select("SELECT account_id, name, email FROM accounts WHERE name = ?", [$name_project]);
        $goals = DB::table('goals')
            ->select('goal_id', 'goal_description')
            ->where( 'account_id' ,$project_id[0]->account_id )
            ->get();
        $this->goals = SupportFunctions::getGoalsFromDb($goals);
        $this->period = SupportFunctions::getDataPeriod($request->all());
        $period_mail = SupportFunctions::getDateFromDateTimeObject($this->period, $mail = true);
        $period_form = SupportFunctions::getDateFromDateTimeObject($this->period);
        $goals_report = explode(',', preg_replace('~^\,~', '', $this->goals));

        $analyticsDataSearch = Analytics::performQuery($this->period, 'ga:adCost, ga:impressions, ga:adClicks, ga:transactions'.$this->goals.',ga:organicSearches',             [
            'dimensions' => 'ga:adDistributionNetwork, ga:campaign',
        ]);

        $result = SupportFunctions::fromQueryToTableGoogleAnalytics($analyticsDataSearch->rows);
        $this->compPeriod = SupportFunctions::getPeriodForComparesin($this->period, $request->all());
        $analyticsDataSearchComparison = Analytics::performQuery($this->compPeriod, 'ga:adCost, ga:impressions, ga:adClicks, ga:transactions'.$this->goals.',ga:organicSearches',             [
            'dimensions' => 'ga:adDistributionNetwork, ga:campaign',
        ]);

        $result_Comparison = SupportFunctions::fromQueryToTableGoogleAnalytics($analyticsDataSearchComparison->rows);
        //отправка письма на почьту
        $mail_checkbox = $request->only(['mail']);
        $result = SupportFunctions::getUnitedResult($result,  $result_Comparison);
        if($mail_checkbox["mail"] == 'on'){
            $data = [
                'results'=> $result,
                'goals' => $goals_report,
                'site_name' =>$goals,
                'period' =>$period_mail,
                'client' =>$project_id
            ];

           Mail::send('emails.report', $data, function($message) use ($data)
            {
                $message->to($data[
                    'client'][0]->email,
                    $data['client'][0]->name
                )->subject('Отчет сайта '.$data['client'][0]->name.' за период c '.$data['period']["start"].' '.' по '.$data['period']["end"]);
            });

            return view('form', ['projects' =>$this->projects]);
        }

        $site_name =$project_id[0]->name;

        return view('table', [
            'results'=> $result,
            'goals' => $goals_report,
            'projects' =>$this->projects,
            'period' => $period_form,
            'site_name' => $site_name
        ]);
    }

    public function getReportAllProjects(Request $request){
        $projects_id = DB::select("SELECT account_id, name, email FROM accounts ");
        $render_results = [];
        foreach ( $projects_id as $project){
            $goals = DB::table('goals')
                ->select('goal_id', 'goal_description')
                ->where( 'account_id' ,$project->account_id )
                ->get();
            $this->goals = SupportFunctions::getGoalsFromDb($goals);
            $this->period = SupportFunctions::getDataPeriod($request->all());
            $period_mail = SupportFunctions::getDateFromDateTimeObject($this->period, $mail = true);
            $period_form = SupportFunctions::getDateFromDateTimeObject($this->period);
            $goals_report = explode(',', preg_replace('~^\,~', '', $this->goals));

            $analyticsDataSearch = Analytics::performQuery($this->period, 'ga:adCost, ga:impressions, ga:adClicks, ga:transactions'.$this->goals.',ga:organicSearches',             [
                'dimensions' => 'ga:adDistributionNetwork, ga:campaign',
            ]);
            $result = SupportFunctions::fromQueryToTableGoogleAnalytics($analyticsDataSearch->rows);
            $this->compPeriod = SupportFunctions::getPeriodForComparesin($this->period, $request->all());
            $analyticsDataSearchComparison = Analytics::performQuery($this->compPeriod, 'ga:adCost, ga:impressions, ga:adClicks, ga:transactions'.$this->goals.',ga:organicSearches',             [
                'dimensions' => 'ga:adDistributionNetwork, ga:campaign',
            ]);

            $result_Comparison = SupportFunctions::fromQueryToTableGoogleAnalytics($analyticsDataSearchComparison->rows);
            //отправка письма на почьту
            $mail_checkbox = $request->only(['mail']);
            $result = SupportFunctions::getUnitedResult($result,  $result_Comparison);
            $render_results[$project->name] = $result;
            $render_goals[$project->name] = $goals_report;
        }

        if($mail_checkbox["mail"] == 'on'){
            $data = [
                'results'=> $render_results,
                'goals' => $render_goals,
                'site_name' =>$goals,
                'period' =>$period_mail,
                'client' =>$project
            ];

            Mail::send('emails.report', $data, function($message) use ($data)
            {
                $message->to($data[
                'client'][0]->email,
                    $data['client'][0]->name
                )->subject('Отчет сайта '.$data['client'][0]->name.' за период c '.$data['period']["start"].' '.' по '.$data['period']["end"]);
            });

            return view('form_total', ['projects' =>$this->projects]);
        }

        $site_name =$project->name;

        return view('table_total', [
            'results'=>  $render_results,
            'goals' => $render_goals,
            'projects' =>$this->projects,
            'period' => $period_form,
            'site_name' => $site_name
        ]);

    }

    public  function  index(){
        return view('form', ['projects' =>$this->projects]);
    }
    public  function  formTotal(){
        return view('form_total', ['projects' =>$this->projects]);
    }






}


